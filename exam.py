'''This programm helps to save, delete, update informations about universities in database. It will help me to choose the best university for me.'''

from PyQt6.QtWidgets import QApplication, QWidget, QPushButton, QLabel, QLineEdit, QVBoxLayout, QHBoxLayout, QFormLayout, QComboBox
from pymongo import MongoClient
import pymongo
import sys

class MenuForm(QWidget):
    def __init__(self):
        super().__init__()
        self.setWindowTitle('PyQt App')
        self.setGeometry(650, 300, 280, 100)
        self.setFixedSize(280, 100)

        self.add_uni = QPushButton("Add university")
        self.uni_list = QPushButton("University list")

        self.layout1 = QVBoxLayout()
        self.layout1.addWidget(self.add_uni)
        self.layout1.addWidget(self.uni_list)
        self.setLayout(self.layout1)

        self.add_uni.clicked.connect(self.add_info)
        self.uni_list.clicked.connect(self.check_info)

    def add_info(self):
        add_menu.show()
        self.close()

    def check_info(self):
        check_menu.show()
        self.close()


class AddUni(QWidget):
    def __init__(self):
        super().__init__()
        self.setWindowTitle('Add university info')
        self.setGeometry(650, 300, 350, 230)
        self.setFixedSize(350, 230)

        self.addLabel = QLabel('<h5>ADD INFO</h5>')
        self.nameF = QLineEdit()
        self.locationF = QLineEdit()
        self.mottoF = QLineEdit()
        self.faculteesF = QLineEdit()
        self.addBtn = QPushButton("ADD")
        self.exitBtn = QPushButton("Exit")

        self.formLayout = QFormLayout()
        self.formLayout.addRow("university name:", self.nameF)
        self.formLayout.addRow("location:", self.locationF)
        self.formLayout.addRow("motto:", self.mottoF)
        self.formLayout.addRow("facultees:", self.faculteesF)
        self.mainLayout = QVBoxLayout()
        self.mainLayout.addWidget(self.addLabel)
        self.mainLayout.addLayout(self.formLayout)
        self.mainLayout.addWidget(self.addBtn)
        self.mainLayout.addWidget(self.exitBtn)
        self.setLayout(self.mainLayout)

        self.addBtn.clicked.connect(self.add)
        self.exitBtn.clicked.connect(self.exitToMenu)

    def exitToMenu(self):
        self.close()
        menu.show()

    def add(self):
        try:
            dbname = get_database()
            users_collection = dbname['universities']
            user = {'university_name': self.nameF.text(), 'location': self.locationF.text(), 'motto': self.mottoF.text(), 'facultees': self.faculteesF.text(), }
            users_collection.insert_one(user)
            self.nameF.clear()
            self.locationF.clear()
            self.mottoF.clear()
            self.faculteesF.clear()
        except Exception as e:
            print(e)


class ShowUni(QWidget):
    def __init__(self):
        super().__init__()
        self.setWindowTitle('Show university info')
        self.setGeometry(650, 300, 350, 200)
        self.setFixedSize(350, 150)

        self.addLabel = QLabel('<h5>SHOW INFO</h5>')
        self.showInfoBtn = QPushButton("Show info")
        self.deleteUniBtn = QPushButton("Delete university")
        self.changeInfoBtn = QPushButton("Change info")
        self.uniList = QComboBox()
        for uni in self.getUniInfo():
            self.uniList.addItem(uni['university_name'])
        self.exitBtn = QPushButton("Exit")

        self.mainLayout = QVBoxLayout()
        self.vLayout = QVBoxLayout()
        self.mainLayout.addWidget(self.addLabel)
        self.mainLayout.addWidget(self.uniList)
        self.hLayout = QHBoxLayout()
        self.hLayout.addWidget(self.showInfoBtn)
        self.hLayout.addWidget(self.deleteUniBtn)
        self.hLayout.addWidget(self.changeInfoBtn)
        self.hLayout.addLayout(self.vLayout)
        self.mainLayout.addLayout(self.hLayout)
        self.mainLayout.addWidget(self.exitBtn)
        self.setLayout(self.mainLayout)

        self.showInfoBtn.clicked.connect(self.showInfo)
        self.deleteUniBtn.clicked.connect(self.deleteUni)
        self.changeInfoBtn.clicked.connect(self.changeInfo)
        self.exitBtn.clicked.connect(self.exitToMenu)

    def showInfo(self):
        self.showInfo = ShowUniInfo(self.unis[self.uniList.currentIndex()])
        self.showInfo.show()

    def getUniInfo(self):
        dbname = get_database()
        users_collection = dbname['universities']
        unis = list(users_collection.find({}))
        self.unis = unis
        return unis

    def deleteUni(self):
        self.deleteUni = DeleteUniInfo(self.unis[self.uniList.currentIndex()])

    def changeInfo(self):
        self.changeInfo = ChangeUniInfo(self.unis[self.uniList.currentIndex()])
        self.changeInfo.show()

    def exitToMenu(self):
        self.close()
        menu.show()


class ShowUniInfo(QWidget):
    def __init__(self, user):
        super().__init__()
        self.setFixedSize(250, 250)
        self.setWindowTitle('Info')
        self.mainLayout = QVBoxLayout()
        self.exitBtn = QPushButton("Exit")
        self.uniName = QLabel(f'<h4 style="background-color: yellow;" >University name:</h4> {user["university_name"]}\n')
        self.location = QLabel(f'<h4 style="background-color: yellow;" >Location:</h4> {user["location"]}\n')
        self.motto = QLabel(f'<h4 style="background-color: yellow;" >Motto:</h4> {user["motto"]}\n')
        self.facultees = QLabel(f'<h4 style="background-color: yellow;" >Facultees:</h4> {user["facultees"]}\n')

        self.mainLayout.addWidget(self.uniName)
        self.mainLayout.addWidget(self.location)
        self.mainLayout.addWidget(self.motto)
        self.mainLayout.addWidget(self.facultees)
        self.mainLayout.addWidget(self.exitBtn)

        self.setLayout(self.mainLayout)
        self.exitBtn.clicked.connect(self.exitToShowMenu)

    def exitToShowMenu(self):
        self.close()


class DeleteUniInfo():
    def __init__(self, user):
        super().__init__()
        dbname = get_database()
        users_collection = dbname['universities']
        users_collection.delete_one(user)


class ChangeUniInfo(QWidget):
    def __init__(self, user):
        super().__init__()
        self.setWindowTitle("Info change window")
        self.setFixedSize(350, 230)

        self.user = user
        self.mainLayout = QVBoxLayout()
        self.buttonL = QHBoxLayout()
        self.exitBtn = QPushButton("Exit")
        self.saveBtn = QPushButton("Save")
        self.uniName = QLabel(f'<h4>University name:</h4>')
        self.location = QLabel(f'<h4>Location:</h4>')
        self.motto = QLabel(f'<h4>Motto:</h4>')
        self.facultees = QLabel(f'<h4>Facultees:</h4>')

        self.nameF = QLineEdit(self.user["university_name"])
        self.locationF = QLineEdit(self.user["location"])
        self.mottoF = QLineEdit(self.user["motto"])
        self.faculteeF = QLineEdit(self.user["facultees"])

        self.mainLayout.addWidget(self.uniName)
        self.mainLayout.addWidget(self.nameF)
        self.mainLayout.addWidget(self.location)
        self.mainLayout.addWidget(self.locationF)
        self.mainLayout.addWidget(self.motto)
        self.mainLayout.addWidget(self.mottoF)
        self.mainLayout.addWidget(self.facultees)
        self.mainLayout.addWidget(self.faculteeF)
        self.buttonL.addWidget(self.saveBtn)
        self.buttonL.addWidget(self.exitBtn)
        self.mainLayout.addLayout(self.buttonL)

        self.setLayout(self.mainLayout)
        self.exitBtn.clicked.connect(self.exitToShowMenu)
        self.saveBtn.clicked.connect(self.infoSave)

    def infoSave(self):
        dbname = get_database()
        users_collection = dbname['universities']
        users_collection.update_one({"university_name": self.user["university_name"]}, {"$set":{"university_name": self.nameF.text()}})
        users_collection.update_one({"university_name": self.user["university_name"]}, {"$set":{"location": self.locationF.text()}})
        users_collection.update_one({"university_name": self.user["university_name"]}, {"$set":{"motto": self.mottoF.text()}})
        users_collection.update_one({"university_name": self.user["university_name"]}, {"$set":{"facultees": self.faculteeF.text()}})

    def exitToShowMenu(self):
        self.close()


def get_database():
    client = MongoClient("mongodb://localhost")
    return client['nurgozel']


app = QApplication(sys.argv)
app.setStyleSheet("QLineEdit { border: 1px solid blue; border-radius: 5px; padding: 3px } QPushButton { border-radius: 3px; border: 1px solid black; padding: 3px } QComboBox { padding: 3px }")
menu = MenuForm()
add_menu = AddUni()
check_menu = ShowUni()

menu.show()
sys.exit(app.exec())